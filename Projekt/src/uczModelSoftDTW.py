import numpy as np
import os
import pickle
import tslearn
import argparse
import time
from tslearn.clustering import TimeSeriesKMeans

def parseArgs():
    parser = argparse.ArgumentParser()

    parser.add_argument("pickleFile", type=str)
    parser.add_argument("n_cluster", type=int)
    parser.add_argument("gamma", type=float)

    parser.add_argument("--out-dir", default="OutDir")

    args=parser.parse_args()
    return args

if __name__=="__main__":
    args=parseArgs()

    with open(args.pickleFile, "rb") as file:
        szeregi=pickle.load(file)

    model=TimeSeriesKMeans(n_clusters=args.n_cluster, metric="softdtw", metric_params={"gamma":args.gamma})
#    model=TimeSeriesKMeans(n_clusters=args.n_cluster, metric="dtw")
    model.fit(szeregi)

    os.makedirs(args.out_dir,exist_ok=True)
    with open(os.path.join(args.out_dir, str(time.time())+"-n_cluster-"+str(args.n_cluster)+"-gamma-"+str(args.gamma)+".pickle"), "wb") as file:
#    with open(os.path.join(args.out_dir, str(time.time())+"-n_cluster-"+str(args.n_cluster)+"-dtw.pickle"), "wb") as file:
        pickle.dump(model, file)
