import os
import pickle
import argparse
import logging

def parseArgs():
    parser=argparse.ArgumentParser()

    parser.add_argument("directory")
    parser.add_argument("--out-dir", default="OutDir")

    return parser.parse_args()

if __name__=="__main__":
    args=parseArgs()
    logging.basicConfig(level=logging.DEBUG)
    listaElementow=os.listdir(args.directory)
    listaKatalogow=[]
    for element in listaElementow:
        if os.path.isdir(os.path.join(args.directory, element)):
            listaKatalogow.append(element)

    logging.info(f"Znalezione katalogi: {listaKatalogow}")

    slownik={}
    for katalog in listaKatalogow:
        for nazwaPliku in os.listdir(os.path.join(args.directory,katalog)):
            with open(os.path.join(args.directory, katalog, nazwaPliku), "r") as plik:
                plik.readline()
                for linia in plik:
                    klucz=linia.split("\t")[0]
                    if klucz in slownik:
                        logging.warning(f"Klucz jest już w słowniku: {klucz}")
                    slownik[klucz]=katalog
        logging.info(f"Zakończono przetwarzanie litery: {katalog}")

    os.makedirs(args.out_dir, exist_ok=True)
    with open(os.path.join(args.out_dir,"katalogFunkcjonalnosciGenow.pickle"), "wb") as plik:
        pickle.dump(slownik, plik)

