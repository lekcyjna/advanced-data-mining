import numpy as np
import os
import pickle
import tslearn
import argparse
import time
from tslearn.metrics import cdist_soft_dtw_normalized, cdist_dtw

def parseArgs():
    parser = argparse.ArgumentParser()

    parser.add_argument("pickleFile", type=str)
    parser.add_argument("gamma", type=float)

    parser.add_argument("--out-dir", default="OutDir")

    args=parser.parse_args()
    return args

if __name__=="__main__":
    args=parseArgs()

    with open(args.pickleFile, "rb") as file:
        szeregi=pickle.load(file)

#    odl=cdist_dtw(szeregi, n_jobs=5)
    odl=cdist_soft_dtw_normalized(szeregi, gamma=args.gamma)

    os.makedirs(args.out_dir,exist_ok=True)
    with open(os.path.join(args.out_dir, "odl-"+str(time.time())+"-gamma-"+str(args.gamma)+".pickle"), "wb") as file:
        pickle.dump(odl, file)
