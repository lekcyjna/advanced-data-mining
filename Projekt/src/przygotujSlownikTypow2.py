import os
import pickle
import argparse
import logging
import re

def parseArgs():
    parser=argparse.ArgumentParser()

    parser.add_argument("directory")
    parser.add_argument("--out-dir", default="OutDir")

    return parser.parse_args()

if __name__=="__main__":
    args=parseArgs()
    logging.basicConfig(level=logging.DEBUG)
    wyr=re.compile("diff_COG\.([A-Z]).*")
    listaElementow=os.listdir(args.directory)
    slownik={}
    for element in listaElementow:
        dopasowanie=wyr.search(element)
        if dopasowanie is None:
            continue
        grupaFunk=dopasowanie.groups()[0]
        with open(os.path.join(args.directory, element), "r") as plik:
            plik.readline()
            for linia in plik:
                klucz=linia.split("\t")[0]
#                if klucz in slownik:
#                    logging.warning(f"Klucz jest już w słowniku: {klucz}")
                slownik[klucz]=grupaFunk
        logging.info(f"Zakończono przetwarzanie litery: {grupaFunk}")

    os.makedirs(args.out_dir, exist_ok=True)
    with open(os.path.join(args.out_dir,"katalogFunkcjonalnosciGenow2.pickle"), "wb") as plik:
        pickle.dump(slownik, plik)

