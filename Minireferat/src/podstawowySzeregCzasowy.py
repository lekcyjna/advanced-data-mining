import numpy as np
import matplotlib.pyplot as plt

X=np.random.randn(120)
Y=np.arange(10)**2/50

X=np.cumsum(X)
X=X[:100]-X[-100:]

plt.plot(X)
plt.show()

plt.plot(Y, "green", linewidth=10)
plt.show()
