#ifndef lista3_hpp
#define lista3_hpp

#include <boost/python.hpp>
#include <boost/python/numpy.hpp>
#include "ZSP/numpy.hpp"

namespace py=boost::python;
namespace np = boost::python::numpy;

py::tuple wygenerujKsztaltyIOdleglosci (np::ndarray szeregiNp, int seed=14,
    unsigned int liczbaKsztaltowDoWygenerowania=128, unsigned int dlugoscKsztaltu=16);

np::ndarray odleglosciMiedzyKsztaltamiISzeregami (np::ndarray szeregiNp, np::ndarray ksztaltyNp);
float policzOdlegloscMiedzyKsztaltemASzeregiem(const np::ndarray&, const np::ndarray&);
np::ndarray odlegloscMiedzySzeregamiAListaKsztaltow(const np::ndarray& szeregiNp, const py::list& listaKsztaltow);

#endif
