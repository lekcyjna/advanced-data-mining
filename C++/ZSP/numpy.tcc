#ifndef ZSP_NUMPY_TCC
#define ZSP_NUMPY_TCC

#include <iostream>

#include "numpy.hpp"

namespace zsp{
namespace numpy{

template<typename npType>
ndarray<npType>::ndarray(::np::ndarray X) 
  : mData((npType*) X.get_data())
  , wiersze(X.shape(0))
  , kolumny( X.get_nd() == 2 ? X.shape(1) : 0)
{ }

template<typename npType>
ndarray<npType>::ndarray(npType* data, unsigned int wiersze, unsigned int kolumny) : 
  mData(data), wiersze(wiersze), kolumny(kolumny)
{ }

template<typename npType>
unsigned int ndarray<npType>::shape(unsigned int ind) const noexcept
{
  switch (ind)
  {
    case 0:
      return wiersze;
    case 1:
      return kolumny;
    default:
      return 0;
  }
}

template<typename npType>
const npType& ndarray<npType>::operator() (int i) const noexcept
{
  return mData[i];
}

template<typename npType>
npType& ndarray<npType>::operator() (int i) noexcept
{
  return mData[i];
}
template<typename npType>
npType ndarray<npType>::operator() (int i, int j) const noexcept
{
  return mData[i*kolumny+j];
}

template<typename npType>
npType& ndarray<npType>::operator() (int i, int j) noexcept
{
  return mData[i*kolumny+j];
}

template<typename npType>
const npType* ndarray<npType>::ptr(unsigned int i) const noexcept
{
  return mData + i;
}

template <typename npType>
ndarray<npType> ndarray<npType>::row(unsigned int rowNumber) const noexcept
{
  npType* data=mData+rowNumber*kolumny;
  return ndarray<npType>{data, kolumny, 0};
}

//template <typename npType>
//::np::ndarray ndarray<npType>::getNdarray() const
//{
//  return mArray;
//}


}
}
#endif
