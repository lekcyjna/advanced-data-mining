#ifndef ZSP_NUMPY_HPP
#define ZSP_NUMPY_HPP

#include <boost/python.hpp>
#include <boost/python/numpy.hpp>

namespace py=boost::python;
namespace np =py::numpy;

namespace zsp{
namespace numpy{

template <typename npType>
class ndarray
{
  public:
    explicit ndarray(::np::ndarray);

    unsigned int shape(unsigned int) const noexcept;

    inline ndarray<npType> row(unsigned int) const noexcept;

//    np::ndarray getNdarray() const ;

//    template <typename ...Ts>
//    npType& operator() (Ts...);

    inline const npType& operator() (int i) const noexcept;
    inline npType& operator() (int i) noexcept;
    inline npType operator() (int i, int j) const noexcept;
    inline npType& operator() (int i, int j) noexcept;

    inline const npType* ptr(unsigned int i) const noexcept;

  private:
    npType* const mData;
//    std::vector<unsigned int> mShape;

    const unsigned int wiersze, kolumny;

    ndarray(npType*, unsigned int, unsigned int);
};

//template <typename npType>
//template <typename ...Ts>
//npType& ndarray<npType>::operator() (Ts... indeksy)
//{
//  int a = (... , indeksy);
//
//}

}
}

#include "numpy.tcc"

#endif
