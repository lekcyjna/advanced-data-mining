#include "lista2.hpp"

#include <cmath>
#include <algorithm>

py::tuple DTWPtr(np::ndarray s, np::ndarray t)
{
  if(((s.get_flags() & np::ndarray::bitflag::C_CONTIGUOUS)  != np::ndarray::bitflag::C_CONTIGUOUS)
      || ((t.get_flags() & np::ndarray::bitflag::C_CONTIGUOUS) != np::ndarray::bitflag::C_CONTIGUOUS))
  {
    throw std::runtime_error("Bledne flagi: "+std::to_string(s.get_flags() & np::ndarray::bitflag::C_CONTIGUOUS));
  }
  if (s.get_dtype() != np::dtype::get_builtin<float>() || t.get_dtype() != np::dtype::get_builtin<float>())
  {
    throw std::runtime_error("Błędny dtype danych. Oczekiwano float");
  }
  int n = s.shape(0);
  int m = t.shape(0);

  float* tablicaS = (float*)s.get_data();
  float* tablicaT = (float*)t.get_data();
  std::vector<float> DTWPop;
  std::vector<float> DTWAkt;
  DTWPop.resize(m,0);
  DTWAkt.resize(m,0);
  np::ndarray kierunekPowrotuArray=np::empty(py::make_tuple(n,m), np::dtype::get_builtin<std::uint8_t>());
  std::uint8_t *kierunekPowrotuPtr=(std::uint8_t*)kierunekPowrotuArray.get_data(); 
  for (int i=0;i<n;i++)
  {
    for(int j=0;j<m;j++)
    {
      float minimalnyPoprzedni;
      std::uint8_t powrot=0;
      if (i==0 and j==0)
      {
        minimalnyPoprzedni=0;
      }
      else  [[likely]]
      {
        minimalnyPoprzedni=1.0e+60;
      }
      if (i>0 and minimalnyPoprzedni>DTWPop[j])
      {
        minimalnyPoprzedni=DTWPop[j];
        powrot=1;
      }
      if(i>0 and j>0 and minimalnyPoprzedni>DTWPop[j-1])
      {
        minimalnyPoprzedni=DTWPop[j-1];
        powrot=2;
      }
      if(j>0 and minimalnyPoprzedni>DTWAkt[j-1])
      {
        minimalnyPoprzedni=DTWAkt[j-1];
        powrot=3;
      }
      kierunekPowrotuPtr[i*m+j]=powrot;
      DTWAkt[j]=minimalnyPoprzedni+std::fabs(tablicaS[i]-tablicaT[j]);
    }
    std::swap(DTWAkt, DTWPop);
  }
  return py::make_tuple(DTWPop[m-1], kierunekPowrotuArray);
}
