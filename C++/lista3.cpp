#include "lista3.hpp"

#include <vector>
#include <span>

#include <random>
#include <algorithm>
#include <x86intrin.h> 
#include <iostream>

#include "ZSP/numpy.hpp"

typedef float npType;

void stworzZbiorKsztaltowDoSprawdzenia(zsp::numpy::ndarray<npType>& szeregi,
    std::default_random_engine& rnd, unsigned int liczbaKsztaltowDoWygenerowania, 
    zsp::numpy::ndarray<npType>& wylosowaneKsztalty, unsigned int dlugoscKsztaltu=16)
{
  std::uniform_int_distribution<unsigned int> UNumerSzeregu {0,szeregi.shape(0)-1};
  std::uniform_int_distribution<unsigned int> UPozycjaWSzeregu {0, szeregi.shape(1)-dlugoscKsztaltu};

  for (unsigned int i=0;i<liczbaKsztaltowDoWygenerowania;i++)
  {
    int n=UNumerSzeregu(rnd);
    int p=UPozycjaWSzeregu(rnd);

    for (unsigned j=0;j<dlugoscKsztaltu;j++)
    {
      wylosowaneKsztalty(i,j)=szeregi(n,p+j);
    }
  }
  return;
}

npType policzOdlegloscMiedzyKsztaltemASzeregiem(const zsp::numpy::ndarray<float>& szereg, const zsp::numpy::ndarray<float>& ksztalt) noexcept
{
  unsigned int dlKsztaltu=ksztalt.shape(0);
  unsigned int koniec=szereg.shape(0)-dlKsztaltu+1; 
  std::vector<float> koszty(koniec);

  for (unsigned int i=0;i<koniec;i++)
  {
    float koszt=0;
    for (unsigned int j=0;j<dlKsztaltu/8;j++)
    {
      __m256 fragmentSzereguVec;
      __m256 ksztaltVec;
      ksztaltVec=_mm256_loadu_ps(ksztalt.ptr(j*8));
      fragmentSzereguVec=_mm256_loadu_ps(szereg.ptr(i+j*8));
      __m256 roznica= _mm256_sub_ps(fragmentSzereguVec, ksztaltVec);
      __m256 kwadrat= _mm256_mul_ps(roznica,roznica);
      __m256 t1 = _mm256_hadd_ps(kwadrat,kwadrat);
      __m256 t2 = _mm256_hadd_ps(t1,t1);
      koszt+=t2[0]+t2[4];
    }
    koszty[i]=koszt;
  }
  return *std::min_element(koszty.cbegin(), koszty.cend());
}

npType policzOdlegloscMiedzyKsztaltemASzeregiem(const np::ndarray& szereg, const np::ndarray& ksztalt) 
{
  if (np::dtype::get_builtin<float>() != szereg.get_dtype())
    throw std::runtime_error("Wrong dtype of szeregiNp");
  if (np::dtype::get_builtin<float>() != ksztalt.get_dtype())
    throw std::runtime_error("Wrong dtype of ksztalty");
  unsigned int dlKsztaltu=ksztalt.shape(0);
  unsigned int koniec=szereg.shape(0)-dlKsztaltu+1; 
  static std::vector<npType> koszty(koniec);

//  std::cerr<<dlKsztaltu<<" "<<koniec<<"\n";
  float* ksztaltPtr=(float*)ksztalt.get_data();
  float* szeregPtr=(float*)szereg.get_data();
  for (unsigned int i=0;i<koniec;i++)
  {
    float koszt=0;
    for (unsigned int j=0;j<dlKsztaltu/8;j++)
    {
      __m256 fragmentSzereguVec;
      __m256 ksztaltVec;
      ksztaltVec=_mm256_loadu_ps(ksztaltPtr+j*8);
      fragmentSzereguVec=_mm256_loadu_ps(szeregPtr+(i+j*8));
      __m256 roznica= _mm256_sub_ps(fragmentSzereguVec, ksztaltVec);
      __m256 kwadrat= _mm256_mul_ps(roznica,roznica);
      __m256 t1 = _mm256_hadd_ps(kwadrat,kwadrat);
      __m256 t2 = _mm256_hadd_ps(t1,t1);
      koszt+=t2[0]+t2[4];
//      std::cerr<<t2[0]+t2[4]<<"\n";
    }
//    std::cerr<<koszt<<"\n";
    koszty[i]=koszt;
  }
  return *std::min_element(koszty.cbegin(), koszty.cend());
}

py::tuple wygenerujKsztaltyIOdleglosci (np::ndarray szeregiNp, int seed,unsigned int liczbaKsztaltowDoWygenerowania,
    unsigned int dlugoscKsztaltu)
{
  static std::default_random_engine rnd;
  rnd.seed(seed);

  if (np::dtype::get_builtin<npType>() != szeregiNp.get_dtype())
    throw std::runtime_error("Wrong dtype of array given to function");
  if (dlugoscKsztaltu%8!=0)
    throw std::runtime_error("Length of shaplet isn't divided by 8.");

  zsp::numpy::ndarray<npType> szeregi {szeregiNp};
  unsigned int liczbaSzeregow=szeregi.shape(0);

  np::ndarray ksztaltyNp =np::empty(py::make_tuple(liczbaKsztaltowDoWygenerowania, dlugoscKsztaltu), np::dtype::get_builtin<npType>());
  zsp::numpy::ndarray<npType> wygenerowaneKsztalty {ksztaltyNp};
  stworzZbiorKsztaltowDoSprawdzenia(szeregi, rnd,liczbaKsztaltowDoWygenerowania, wygenerowaneKsztalty, dlugoscKsztaltu);

  np::ndarray odleglosciNp=np::zeros(py::make_tuple(liczbaSzeregow, liczbaKsztaltowDoWygenerowania), np::dtype::get_builtin<npType>());
  zsp::numpy::ndarray<npType> odleglosci{odleglosciNp};
  for (unsigned int i=0;i<liczbaSzeregow;i++)
  {
    for(unsigned int j=0;j<liczbaKsztaltowDoWygenerowania;j++)
    {
      odleglosci(i,j)=policzOdlegloscMiedzyKsztaltemASzeregiem(szeregi.row(i), wygenerowaneKsztalty.row(j));
    }
  }

  return py::make_tuple(odleglosciNp, ksztaltyNp);
}

np::ndarray odleglosciMiedzyKsztaltamiISzeregami (np::ndarray szeregiNp, np::ndarray ksztaltyNp)
{
  if (np::dtype::get_builtin<npType>() != szeregiNp.get_dtype())
    throw std::runtime_error("Wrong dtype of szeregiNp");
  if (np::dtype::get_builtin<npType>() != ksztaltyNp.get_dtype())
    throw std::runtime_error("Wrong dtype of ksztalty");

  zsp::numpy::ndarray<npType> szeregi {szeregiNp};
  unsigned int liczbaSzeregow=szeregi.shape(0);

  zsp::numpy::ndarray<npType> ksztalty {ksztaltyNp};
  unsigned int liczbaKsztaltow=ksztalty.shape(0);
  unsigned int dlugoscKsztaltu=ksztalty.shape(1);

  if (dlugoscKsztaltu%8!=0)
    throw std::runtime_error("Length of shaplet isn't divided by 8.");

  np::ndarray odleglosciNp=np::zeros(py::make_tuple(liczbaSzeregow, liczbaKsztaltow), np::dtype::get_builtin<npType>());
  zsp::numpy::ndarray<npType> odleglosci{odleglosciNp};
  for (unsigned int i=0;i<liczbaSzeregow;i++)
  {
    for(unsigned int j=0;j<liczbaKsztaltow;j++)
    {
      odleglosci(i,j)=policzOdlegloscMiedzyKsztaltemASzeregiem(szeregi.row(i), ksztalty.row(j));
    }
  }

  return odleglosciNp;
}

np::ndarray odlegloscMiedzySzeregamiAListaKsztaltow(const np::ndarray& szeregiNp, const py::list& listaKsztaltow)
{
  if (np::dtype::get_builtin<float>() != szeregiNp.get_dtype())
    throw std::runtime_error("Wrong dtype of szeregiNp");

  zsp::numpy::ndarray<npType> szeregi {szeregiNp};
  unsigned int liczbaSzeregow=szeregi.shape(0);

  unsigned int liczbaKsztaltow=py::len(listaKsztaltow);

  np::ndarray odleglosciNp=np::zeros(py::make_tuple(liczbaSzeregow, liczbaKsztaltow), np::dtype::get_builtin<npType>());
  zsp::numpy::ndarray<npType> odleglosci{odleglosciNp};

  py::stl_input_iterator<np::ndarray> iterKsztalty{listaKsztaltow};
  for (unsigned int j=0;j<liczbaKsztaltow;j++)
  {
    const zsp::numpy::ndarray<float> ksztalt{*iterKsztalty};
    ++iterKsztalty;
    for(unsigned int i=0;i<liczbaSzeregow;i++)
    {
      float wyn=policzOdlegloscMiedzyKsztaltemASzeregiem(szeregi.row(i), ksztalt);
      odleglosci(i,j)=wyn;
    }
  }
  return odleglosciNp;
}

