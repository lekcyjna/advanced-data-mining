#ifndef lista2_hpp
#define lista2_hpp

#include <boost/python.hpp>
#include <boost/python/numpy.hpp>

namespace py=boost::python;
namespace np = boost::python::numpy;
py::tuple DTWPtr(np::ndarray s, np::ndarray t);

#endif
