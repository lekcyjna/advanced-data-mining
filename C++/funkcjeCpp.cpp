#include <boost/python.hpp>
#include <boost/python/detail/defaults_gen.hpp>
#include <boost/python/numpy.hpp>

#include "lista2.hpp"
#include "lista3.hpp"

namespace py=boost::python;
namespace np = boost::python::numpy;

int flagi(np::ndarray macierz)
{
  return (int)macierz.get_flags();
}

BOOST_PYTHON_FUNCTION_OVERLOADS(wygenerujKsztaltyIOdleglosci_overloads, wygenerujKsztaltyIOdleglosci, 1,4)

BOOST_PYTHON_MODULE(funkcjeCpp)
{
  np::initialize();
  py::def("DTWPtr", DTWPtr);
  py::def("flagi", flagi);
  py::def("wygenerujKsztaltyIOdleglosci",wygenerujKsztaltyIOdleglosci,wygenerujKsztaltyIOdleglosci_overloads());
  py::def("odleglosciMiedzyKsztaltamiISzeregami", odleglosciMiedzyKsztaltamiISzeregami);
  py::def("policzOdlegloscMiedzyKsztaltemASzeregiem",policzOdlegloscMiedzyKsztaltemASzeregiem);
  py::def("odlegloscMiedzySzeregamiAListaKsztaltow", odlegloscMiedzySzeregamiAListaKsztaltow);
}
